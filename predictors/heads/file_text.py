import fileinput

import os
import glob
for filename in glob.glob('*.py'):
	text_to_search= "from "
	replacement_text= "from "
	with fileinput.FileInput(filename, inplace=True) as file:
	    for line in file:
	        print(line.replace(text_to_search, replacement_text), end='')
